#!/bin/bash
#This automated installation script let you install an instance of Storm Node (Mono-Cluster)
# By Knut (adrien.stefanski@gmail.com)
#
#



#
#Requirement for reporting
#

sudo apt-get install mysql-server mysql-client
sudo /etc/init.d/mysql start
sudo apt-get install apache2
sudo apt-get install php5


#Install requirement
#
#
# Install Java JDK
#
cd /tmp
rm *
wget  https://www.dropbox.com/s/w4liwrkstemsjvr/jdk-8u111-linux-i586.tar.gz
tar -xvzf jdk-8u111-linux-i586.tar.gz
# sudo mkdir /opt/jdk
sudo rm -rf /opt/jdk/
sudo mv jdk1.8.0_111 /opt/jdk/

export JAVA_HOME=/opt/jdk/

#
# Install Zookeeper
#

wget http://apache.mediamirrors.org/zookeeper/zookeeper-3.4.11/zookeeper-3.4.11.tar.gz
tar -zvxf zookeeper-3.4.11.tar.gz
sudo rm -rf /opt/zookeeper
#sudo mkdir /opt/zookeeper
sudo mv zookeeper-3.4.11 /opt/zookeeper/

#
# Configuration Zookeeper
#

sudo mkdir /opt/zookeeper/data
cat > /opt/zookeeper/conf/zoo.cfg << EOF
tickTime=2000
dataDir=/path/to/zookeeper/data
clientPort=2181
initLimit=5
syncLimit=2
EOF


/opt/zookeeper/bin/zkServer.sh start

#
# Optionnal Zookeeper Command
#

#Accès au CLI
#/opt/zookeeper/bin/zkCli.sh
#Stop Zookeeper
#/opt/zookeeper/bin/zkServer.sh stop


#
# Install Apache STORM
#

wget http://apache.mirrors.ovh.net/ftp.apache.org/dist/storm/apache-storm-1.0.2/apache-storm-1.0.2.tar.gz
tar -zvxf apache-storm-1.0.2.tar.gz
sudo rm -rf /opt/storm/
# sudo mkdir /opt/storm

sudo mv apache-storm-1.0.2 /opt/storm/
sudo mkdir /opt/storm/data


#
# Configuration Apache STORM
#
cat > /opt/storm/conf/storm.yaml << EOF
storm.zookeeper.servers:
- "localhost"
storm.local.dir: ?/path/to/storm/data(any path)?
nimbus.host: "localhost"
supervisor.slots.ports:
- 6700
- 6701
- 6702
- 6703
EOF

#
# Lancement de STORM
#
/opt/storm/bin/storm nimbus
/opt/storm/bin/storm supervisor
/opt/storm/bin/storm ui
